const express = require('express');
const app = express();
const PORT = 8080;
const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'universe',
    password: '123456',
    port: 5432,
  });

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

pool.connect((err, client, release) => {
    if (err) {
      return console.error('Error acquiring client', err.stack)
    }
    client.query('SELECT NOW()', (err, result) => {
      release()
      if (err) {
        return console.error('Error executing query', err.stack)
      }
      console.log(result.rows)
    })
  });

app.get('/api/v1/planets', (req, res) => {
    pool
    .query('SELECT * FROM planets ORDER BY id ASC')
    .then(response => res.status(200).json(response.rows))
    .catch(err => console.error('Error executing query', err.stack))
});

app.post('/api/v1/planets/add', (req, res) => {
    const {id, name, size, date_discovered } = req.body;

    pool
    .query(`INSERT INTO planets (id, name, size, date_discovered) VALUES (${id}, '${name}', ${size}, '${date_discovered}') RETURNING *`)
    .then(response => res.status(200).json(response.rows[0]))
    .catch(err => console.error('Error executing query', err.stack))
});

app.delete('/api/v1/planets/delete', (req, res) => {
    const { id } = req.query;

    pool
    .query(`DELETE FROM planets WHERE id=${id} RETURNING *`)
    .then(response => res.status(200).json(response.rows[0]))
    .catch(err => console.error('Error executing query', err.stack))
});

app.put('/api/v1/planets/update', (req, res) => {

    const { id, name, size, date_discovered } = req.body;

    pool
    .query(`UPDATE planets SET id=${id}, name='${name}', size=${size}, date_discovered='${date_discovered}' WHERE id=${id} RETURNING *`)
    .then(response => res.status(200).json(response.rows[0]))
    .catch(err => console.error('Error executing query', err.stack))
});

app.listen(PORT, ()=>{
    console.log(`Started express server on port ${PORT}`);
});